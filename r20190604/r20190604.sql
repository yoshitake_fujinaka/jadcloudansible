-- issue-1347
ALTER TABLE t_shipment_status_history
ADD COLUMN DELIVERY_PLAN_DATE     DATE COMMENT '配達予定日' AFTER `TERMINAL_CD`,
ADD COLUMN DELIVERY_TIME_FROM     CHAR(2) COMMENT '配達予定時間帯FROM' AFTER `DELIVERY_PLAN_DATE`,
ADD COLUMN DELIVERY_TIME_TO       CHAR(2) COMMENT '配達予定時間帯TO' AFTER `DELIVERY_TIME_FROM`
;
ALTER TABLE old_shipment_status_history
ADD COLUMN DELIVERY_PLAN_DATE     DATE COMMENT '配達予定日' AFTER `TERMINAL_CD`,
ADD COLUMN DELIVERY_TIME_FROM     CHAR(2) COMMENT '配達予定時間帯FROM' AFTER `DELIVERY_PLAN_DATE`,
ADD COLUMN DELIVERY_TIME_TO       CHAR(2) COMMENT '配達予定時間帯TO' AFTER `DELIVERY_TIME_FROM`
;

-- issue-1266
ALTER TABLE m_shipper
ADD COLUMN SLIP_LOGO     VARCHAR(300) COMMENT '伝票ロゴ' AFTER `INIT_SHIPMENT_STATUS`
;

-- issue-1270
-- 荷物サイズの変更、及び追加
UPDATE tbl_data_convert SET val_to = '1' WHERE field_name = 'SIZE' and id = 10;
UPDATE tbl_data_convert SET val_to = '2' WHERE field_name = 'SIZE' and id = 11;
INSERT INTO tbl_data_convert (`field_name`, `id`, `val_from`, `val_to`) VALUES ('SIZE', 12, '12', '3');

-- issue-1466 prod環境
INSERT INTO s_env(KEY_NAME, VALUE) VALUES ('VOUCHER_VERSION', '2');