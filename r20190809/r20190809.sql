-- issue-1853 ”出荷ステータス（配達実績）” に、”10：調査中”を追加（DELETE / INSERT）
DELETE FROM tbl_data_convert WHERE field_name = 'SHIPMENT_STATUS_DELIVERY_RESULT'
;

INSERT INTO tbl_data_convert( field_name, id, val_from, val_to, DELETE_DATE )
VALUES
( 'SHIPMENT_STATUS_DELIVERY_RESULT', 1, '5', '持ち出し', null ),
( 'SHIPMENT_STATUS_DELIVERY_RESULT', 2, '6', '配達完了', null ),
( 'SHIPMENT_STATUS_DELIVERY_RESULT', 3, '7', '不在', null ),
( 'SHIPMENT_STATUS_DELIVERY_RESULT', 4, '8', '配達拒否', null ),
( 'SHIPMENT_STATUS_DELIVERY_RESULT', 5, '10', '調査中', null ),
( 'SHIPMENT_STATUS_DELIVERY_RESULT', 6, '11', 'センター保管（持戻）', null ),
( 'SHIPMENT_STATUS_DELIVERY_RESULT', 7, '12', '指定日保管（持戻）', null ),
( 'SHIPMENT_STATUS_DELIVERY_RESULT', 8, '35', 'センター保管（再配達／持戻）', '2019/07/07' ),
( 'SHIPMENT_STATUS_DELIVERY_RESULT', 9, '36', '指定日保管（再配達／持戻）', '2019/07/07' )
;
