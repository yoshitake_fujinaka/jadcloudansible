# ansible/roles/common_goofys

# 概要

ansible/roles/common_goofysは、goofysをインストールするタスクを定義します。


## goofys とは
goofys とはサーバから S3 バケットをファイルシステムのようにマウントして、OS上からディレクトリの様に使用できるソリューションです。

https://github.com/kahing/goofys


