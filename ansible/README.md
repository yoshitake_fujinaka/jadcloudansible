# 使い方

## Ansibleをはじめて使う方は以下のサイトを一読ください。

https://qiita.com/t_nakayama0714/items/fe55ee56d6446f67113c

https://qiita.com/legitwhiz/items/672b8c6aa569b118e7a5#_reference-b04f8bb14da63d2ecaf0

https://qiita.com/yamazon/items/26ebfec2f076db6b7c3f#_reference-e02934ea9ad58d835bb3

# 各環境について

## 環境（ステージ=stage）

環境には以下の種類があります。

- 個人開発環境
- 開発環境
- ステージング環境
- プロダクション環境

それぞれの呼称(ステージ名=stage)は以下の通りです。

- 個人開発環境　local
- 開発環境　dev
- ステージング環境　stg
- プロダクション環境 prod

例えば、環境設定ファイルや命名にステージ名を使用します。

例）

- .env.dev ・・・　開発環境用
- .env.prod ・・・　プロダクション環境用

- jad-dev-web01 ・・・　開発環境のWebサーバー１号機

# 環境へのアクセス方法について

## SSHでの接続

sshの設定ファイルに下記を追加します。
必要に応じてパラメータシートを参照して、追加修正してください。

~/.ssh/crooz.pemは、パラメータシートにあるキーから作成

~/.ssh/821019812189_LightsailDefaultKey-ap-northeast-1.pemは、lightsailからダウンロードします。

```~/.ssh/config
Host jad_fumidai
  HostName 172.17.0.10
  User y.fujinaka

Host jad_stg_db
  HostName 54.249.33.138
  ProxyCommand ssh -W %h:%p jad_fumidai

Host jad_stg_web01
  HostName 54.249.33.138
  User centos
  IdentityFile ~/.ssh/crooz.pem
  ServerAliveInterval 60
  ProxyCommand sshpass -p <y.fujinakaのパスワード> ssh jad_fumidai -W %h:%p

Host jad_prod_web01
  HostName 172.26.8.182
  User centos
  IdentityFile ~/.ssh/crooz.pem
  ServerAliveInterval 60
  ProxyCommand ssh -W %h:%p jad_stg_web01

Host jad_prod_web02
  HostName 172.26.6.117
  User centos
  IdentityFile ~/.ssh/crooz.pem
  ServerAliveInterval 60
  ProxyCommand ssh -W %h:%p jad_stg_web01

Host jad_dev_web01
  HostName 54.250.154.172
  User centos
  IdentityFile ~/.ssh/821019812189_LightsailDefaultKey-ap-northeast-1.pem
  ServerAliveInterval 60

Host jad_dev_web02
  HostName 18.182.63.151
  User centos
  IdentityFile ~/.ssh/821019812189_LightsailDefaultKey-ap-northeast-1.pem
  ServerAliveInterval 60

Host jad_dev_batch01
  HostName 18.182.63.151
  User centos
  IdentityFile ~/.ssh/821019812189_LightsailDefaultKey-ap-northeast-1.pem
  ServerAliveInterval 60
```

あとは、Host名を指定してsshします。

```
$ ssh jad_dev_web02
```

stgやprodに接続する場合は、VPNへの接続が必要です。

## DBへの接続について

DBへは、sshトネリング接続でのみ接続可能です。

dev環境では、jad_dev_web01経由で接続します。

stg/prod環境では、jad_stg_db経由で接続します。

ツールによっては、ProxyCommandが機能しないものもあるので
jad_stg_dbを使います。
<藤中さんのパスワード>の入力が求められます。





