-- issue-1853 insert_tbl_data_convert.sql 
-- ”出荷ステータス（配達実績）” に、”10：調査中”を追加（DELETE / INSERT）
DELETE FROM tbl_data_convert WHERE field_name = 'SHIPMENT_STATUS_DELIVERY_RESULT';
INSERT INTO tbl_data_convert (`field_name`, `id`, `val_from`, `val_to`) VALUES 
('SHIPMENT_STATUS_DELIVERY_RESULT', 1, '5', '持ち出し'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 2, '6', '配達完了'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 3, '7', '不在'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 4, '8', '配達拒否'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 5, '10', '調査中'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 6, '11', 'センター保管（持戻）'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 7, '12', '指定日保管（持戻）'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 8, '35', 'センター保管（再配達／持戻）'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 9, '36', '指定日保管（再配達／持戻）')
;

-- issue-1992 alter_table_t_shipment_status_history.sql
-- メモの桁数を300→1500に変更
ALTER TABLE `t_shipment_status_history` MODIFY MEMO VARCHAR(1500) COMMENT 'メモ';
ALTER TABLE `old_shipment_status_history` MODIFY MEMO VARCHAR(1500) COMMENT 'メモ';

-- issue-1992 alter_table_t_shipment.sql
-- メモの桁数を300→1500に変更
ALTER TABLE `t_shipment` MODIFY MEMO VARCHAR(1500) COMMENT 'メモ';
ALTER TABLE `old_shipment` MODIFY MEMO VARCHAR(1500) COMMENT 'メモ';

-- issue-1992 update_tbl_headers.sql
-- メモの桁数チェックを300 → 1500に変更
UPDATE tbl_headers_shipper SET digit_val = 1500 WHERE id = 80;
UPDATE tbl_headers_center SET digit_val = 1500 WHERE id = 80;

-- issue-1992 insert_tbl_data_convert.sql
-- ”出荷ステータス” に、”13：荷物事故”を追加（DELETE / INSERT）
DELETE FROM tbl_data_convert WHERE field_name = 'SHIPMENT_STATUS';
INSERT INTO tbl_data_convert (`field_name`, `id`, `val_from`, `val_to`) VALUES 
('SHIPMENT_STATUS', 1, '0', '未出荷'),
('SHIPMENT_STATUS', 2, '1', 'S荷受'),
('SHIPMENT_STATUS', 3, '2', '荷受確定'),
('SHIPMENT_STATUS', 4, '3', 'センター保管'),
('SHIPMENT_STATUS', 5, '4', '指定日保管'),
('SHIPMENT_STATUS', 6, '5', '持ち出し'),
('SHIPMENT_STATUS', 7, '6', '配達完了'),
('SHIPMENT_STATUS', 8, '7', '不在'),
('SHIPMENT_STATUS', 9, '8', '配達拒否'),
('SHIPMENT_STATUS', 10, '9', '返品'),
('SHIPMENT_STATUS', 11, '10', '調査中'),
('SHIPMENT_STATUS', 12, '11', 'センター保管（持戻）'),
('SHIPMENT_STATUS', 13, '12', '指定日保管（持戻）'),
('SHIPMENT_STATUS', 14, '13', '荷物事故'),
('SHIPMENT_STATUS', 15, '31', '指定日保管（再配達）'),
('SHIPMENT_STATUS', 16, '32', '持ち出し（再配達）'),
('SHIPMENT_STATUS', 17, '33', '不在（再配達）'),
('SHIPMENT_STATUS', 18, '34', '調査中（再配達）'),
('SHIPMENT_STATUS', 19, '35', 'センター保管（再配達／持戻）'),
('SHIPMENT_STATUS', 20, '36', '指定日保管（再配達／持戻）')
;

-- ”荷物状況” に、”13：調査中（荷物事故）”を追加（DELETE / INSERT）
DELETE FROM tbl_data_convert WHERE field_name = 'PACKAGE_STATUS';
INSERT INTO tbl_data_convert (`field_name`, `id`, `val_from`, `val_to`) VALUES 
('PACKAGE_STATUS', 1, '0', '未出荷'),
('PACKAGE_STATUS', 2, '1', '出荷依頼'),
('PACKAGE_STATUS', 3, '2', '荷物受付'),
('PACKAGE_STATUS', 4, '3', '受付センター保管'),
('PACKAGE_STATUS', 5, '4', '受付センター保管'),
('PACKAGE_STATUS', 6, '5', '発送'),
('PACKAGE_STATUS', 7, '6', '配達完了'),
('PACKAGE_STATUS', 8, '7', '持ち帰り（不在）'),
('PACKAGE_STATUS', 9, '8', '配達拒否'),
('PACKAGE_STATUS', 10, '9', '返品'),
('PACKAGE_STATUS', 11, '10', '調査中'),
('PACKAGE_STATUS', 12, '11', '受付センター保管（持戻）'),
('PACKAGE_STATUS', 13, '12', '受付センター保管（持戻）'),
('PACKAGE_STATUS', 14, '13', '調査中'),
('PACKAGE_STATUS', 15, '31', '受付センター保管（再配達待ち）'),
('PACKAGE_STATUS', 16, '32', '発送（再配達）'),
('PACKAGE_STATUS', 17, '33', '持ち帰り（再配達受付）'),
('PACKAGE_STATUS', 18, '34', '調査中（再配達受付）'),
('PACKAGE_STATUS', 19, '35', '受付センター保管（再配達待ち／持戻）'),
('PACKAGE_STATUS', 20, '36', '受付センター保管（再配達待ち／持戻）')
;

-- issue-2000 alter_table_tbl_data_convert.sql
-- 「削除日」を追加
ALTER TABLE tbl_data_convert ADD COLUMN DELETE_DATE DATE COMMENT '削除日' AFTER `val_to`;

-- 削除対象となる再配達のコード値に「削除日」を設定
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS' AND val_from = '31';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS' AND val_from = '32';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS' AND val_from = '33';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS' AND val_from = '34';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS' AND val_from = '35';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS' AND val_from = '36';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS_SALES_UNFINALIZED' AND val_from = '31';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS_SALES_UNFINALIZED' AND val_from = '32';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS_SALES_UNFINALIZED' AND val_from = '33';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS_SALES_UNFINALIZED' AND val_from = '34';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS_SALES_UNFINALIZED' AND val_from = '35';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS_SALES_UNFINALIZED' AND val_from = '36';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS_DELIVERY_RESULT' AND val_from = '35';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'SHIPMENT_STATUS_DELIVERY_RESULT' AND val_from = '36';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'PACKAGE_STATUS' AND val_from = '31';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'PACKAGE_STATUS' AND val_from = '32';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'PACKAGE_STATUS' AND val_from = '33';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'PACKAGE_STATUS' AND val_from = '34';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'PACKAGE_STATUS' AND val_from = '35';
UPDATE tbl_data_convert SET DELETE_DATE = '2019/07/07' WHERE field_name = 'PACKAGE_STATUS' AND val_from = '36';
