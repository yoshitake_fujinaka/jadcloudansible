-- issue-1388
-- 出荷ステータス（配達実績）の追加
INSERT INTO tbl_data_convert (`field_name`, `id`, `val_from`, `val_to`) VALUES 
('SHIPMENT_STATUS_DELIVERY_RESULT', 1, '5', '持ち出し'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 2, '6', '配達完了'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 3, '7', '不在'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 4, '8', '配達拒否'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 5, '11', 'センター保管（持戻）'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 6, '12', '指定日保管（持戻）'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 7, '35', 'センター保管（再配達／持戻）'),
('SHIPMENT_STATUS_DELIVERY_RESULT', 8, '36', '指定日保管（再配達／持戻）');

-- issue-1461
-- 送り状種別の追加（DELETE / INSERT）
DELETE FROM tbl_data_convert WHERE field_name = 'SEND_TYPE';
INSERT INTO tbl_data_convert (`field_name`, `id`, `val_from`, `val_to`) VALUES 
('SEND_TYPE', 1, '0', '発払'),
('SEND_TYPE', 2, '2', 'コレクト'),
('SEND_TYPE', 3, '3', 'メール便'),
('SEND_TYPE', 4, '4', 'タイム'),
('SEND_TYPE', 5, '5', '着払'),
('SEND_TYPE', 6, '6', 'メール便速達サービス'),
('SEND_TYPE', 7, '7', 'ネコポス'),
('SEND_TYPE', 8, '8', '集配'),
('SEND_TYPE', 9, '9', '集荷');