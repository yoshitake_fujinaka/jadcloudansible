-- issue-1212 01.change_table_s_env.sql
DELETE FROM s_env WHERE KEY_NAME = 'LOGFILE_NOT_COMPRESS';
INSERT INTO s_env(KEY_NAME, VALUE) VALUES ('LOGFILE_NOT_COMPRESS', '7');
DELETE FROM s_env WHERE KEY_NAME = 'LOGFILE_MAX';
INSERT INTO s_env(KEY_NAME, VALUE) VALUES ('LOGFILE_MAX', '30');

-- issue-1212 02.insert_S_BATCH_RUN_MANAGE.sql
DELETE FROM s_batch_run_manage WHERE BATCH_ID = 'LOGFILE_COMPRESS_DELETE';
INSERT INTO s_batch_run_manage(BATCH_ID) VALUES ('LOGFILE_COMPRESS_DELETE');

-- issue-1216 01.alter_index_t_shipment_status_history.sql
ALTER TABLE t_shipment_status_history DROP INDEX IDX_T03_DELIVERY_KBN;
ALTER TABLE t_shipment_status_history DROP INDEX IDX_T03_UPDATE_TIMESTAMP;
ALTER TABLE t_shipment_status_history
ADD INDEX IDX_T03_DELIVERY_KBN(DELIVERY_KBN),
ADD INDEX IDX_T03_UPDATE_TIMESTAMP(UPDATE_TIMESTAMP);

-- issue-1216 S_01.create_table_tbl_data_convert.sql
DROP TABLE IF EXISTS tbl_data_convert;
CREATE TABLE tbl_data_convert (
  field_name             VARCHAR(255) NOT NULL COMMENT '項目名',
  id                     int(5) UNSIGNED NOT NULL COMMENT 'シーケンス',
  val_from               VARCHAR(255) NOT NULL COMMENT 'コード値',
  val_to                 VARCHAR(255) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY(field_name, id)
);
INSERT INTO tbl_data_convert (`field_name`, `id`, `val_from`, `val_to`) VALUES
('SHIPMENT_STATUS', 1, '0', '未出荷'),
('SHIPMENT_STATUS', 2, '1', 'S荷受'),
('SHIPMENT_STATUS', 3, '2', '荷受確定'),
('SHIPMENT_STATUS', 4, '3', 'センター保管'),
('SHIPMENT_STATUS', 5, '4', '指定日保管'),
('SHIPMENT_STATUS', 6, '5', '持ち出し'),
('SHIPMENT_STATUS', 7, '6', '配達完了'),
('SHIPMENT_STATUS', 8, '7', '不在'),
('SHIPMENT_STATUS', 9, '8', '配達拒否'),
('SHIPMENT_STATUS', 10, '9', '返品'),
('SHIPMENT_STATUS', 11, '10', '調査中'),
('SHIPMENT_STATUS', 12, '11', 'センター保管（持戻）'),
('SHIPMENT_STATUS', 13, '12', '指定日保管（持戻）'),
('SHIPMENT_STATUS', 14, '31', '指定日保管（再配達）'),
('SHIPMENT_STATUS', 15, '32', '持ち出し（再配達）'),
('SHIPMENT_STATUS', 16, '33', '不在（再配達）'),
('SHIPMENT_STATUS', 17, '34', '調査中（再配達）'),
('SHIPMENT_STATUS', 18, '35', 'センター保管（再配達／持戻）'),
('SHIPMENT_STATUS', 19, '36', '指定日保管（再配達／持戻）'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 1, '0', '未出荷'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 2, '1', 'S荷受'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 3, '2', '荷受確定'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 4, '3', 'センター保管'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 5, '4', '指定日保管'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 6, '5', '持ち出し'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 7, '7', '不在'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 8, '10', '調査中'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 9, '11', 'センター保管（持戻）'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 10, '12', '指定日保管（持戻）'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 11, '31', '指定日保管（再配達）'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 12, '32', '持ち出し（再配達）'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 13, '33', '不在（再配達）'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 14, '34', '調査中（再配達）'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 15, '35', 'センター保管（再配達／持戻）'),
('SHIPMENT_STATUS_SALES_UNFINALIZED', 16, '36', '指定日保管（再配達／持戻）'),
('PACKAGE_STATUS', 1, '0', '未出荷'),
('PACKAGE_STATUS', 2, '1', '荷物受付'),
('PACKAGE_STATUS', 3, '2', '荷物受付'),
('PACKAGE_STATUS', 4, '3', '受付センター保管'),
('PACKAGE_STATUS', 5, '4', '受付センター保管'),
('PACKAGE_STATUS', 6, '5', '発送'),
('PACKAGE_STATUS', 7, '6', '配達完了'),
('PACKAGE_STATUS', 8, '7', '持ち帰り（不在）'),
('PACKAGE_STATUS', 9, '8', '配達拒否'),
('PACKAGE_STATUS', 10, '9', '返品'),
('PACKAGE_STATUS', 11, '10', '調査中'),
('PACKAGE_STATUS', 12, '11', '受付センター保管（持戻）'),
('PACKAGE_STATUS', 13, '12', '受付センター保管（持戻）'),
('PACKAGE_STATUS', 14, '31', '受付センター保管（再配達待ち）'),
('PACKAGE_STATUS', 15, '32', '発送（再配達）'),
('PACKAGE_STATUS', 16, '33', '持ち帰り（再配達受付）'),
('PACKAGE_STATUS', 17, '34', '調査中（再配達受付）'),
('PACKAGE_STATUS', 18, '35', '受付センター保管（再配達待ち／持戻）'),
('PACKAGE_STATUS', 19, '36', '受付センター保管（再配達待ち／持戻）'),
('DELIVERY_KBN', 1, '1', '受取'),
('DELIVERY_KBN', 2, '2', '宅配BOX'),
('DELIVERY_KBN', 3, '3', '置き配達'),
('DELIVERY_KBN', 4, '4', 'ポスト投函'),
('SIZE', 1, '1', '60'),
('SIZE', 2, '2', '80'),
('SIZE', 3, '3', '100'),
('SIZE', 4, '4', '120'),
('SIZE', 5, '5', '140'),
('SIZE', 6, '6', '160'),
('SIZE', 7, '7', '180'),
('SIZE', 8, '8', '200'),
('SIZE', 9, '9', '200以上'),
('SIZE', 10, '10', '10'),
('SIZE', 11, '11', '20'),
('SEND_TYPE', 1, '0', '発払'),
('SEND_TYPE', 2, '2', 'コレクト'),
('SEND_TYPE', 3, '5', '着払'),
('SEND_TYPE', 4, '8', '集配'),
('SEND_TYPE', 5, '9', '集荷'),
('COOL_TYPE', 1, '0', '通常'),
('COOL_TYPE', 2, '1', 'クール冷凍'),
('COOL_TYPE', 3, '2', 'クール冷蔵'),
('COOL_TYPE', 4, '3', 'チルド'),
('COOL_TYPE', 5, '4', 'セキュリティ'),
('ABSENCE_REASON_CD', 1, '1', '不在'),
('ABSENCE_REASON_CD', 2, '2', '宅配BOX空きなし・宅配BOXサイズオーバー'),
('SURVEY_REASON_CD', 1, '3', '住所、お客様名不明'),
('SURVEY_REASON_CD', 2, '4', 'その他'),
('DELIVERY_TICKET_EDIT_FLG', 1, '0', '不可'),
('DELIVERY_TICKET_EDIT_FLG', 2, '1', '可'),
('ADMIN_AUTHORITY', 1, '0', '権限なし'),
('ADMIN_AUTHORITY', 2, '1', '権限あり'),
('EMPLOYEE_TYPE', 1, '1', '社員'),
('EMPLOYEE_TYPE', 2, '2', 'パートナー'),
('EMPLOYEE_TYPE', 3, '3', '臨時'),
('AFFILIATION_TYPE', 1, '1', 'JAD'),
('AFFILIATION_TYPE', 2, '2', 'パートナー'),
('APPS_TYPE', 1, '1', 'ドライバーアプリ'),
('APPS_TYPE', 2, '2', 'センターアプリ'),
('RE_DELIVERY_REQUEST_MEANS', 1, '1', 'Webサイト'),
('RE_DELIVERY_REQUEST_MEANS', 2, '2', 'IVR'),
('REDELIVERY_MAIL_RECEIVE_FLG', 1, '0', '受信しない'),
('REDELIVERY_MAIL_RECEIVE_FLG', 2, '1', '受信する'),
('OFFICE_STOP', 1, '0', '利用しない'),
('OFFICE_STOP', 2, '1', '利用する');

-- issue-1358 issue-1358.sql
alter table tbl_headers_center drop text_align;
alter table tbl_headers_center add text_align integer not null default 0 COMMENT '0: center 1: left 2: right';
update tbl_headers_center set text_align = 1 where id = 14 and en_name = 'DELIVERY_ADDRESS1';
update tbl_headers_center set text_align = 1 where id = 15 and en_name = 'DELIVERY_ADDRESS2';
update tbl_headers_center set text_align = 1 where id = 18 and en_name = 'DELIVERY_COMPANY1';
update tbl_headers_center set text_align = 1 where id = 19 and en_name = 'DELIVERY_COMPANY2';
update tbl_headers_center set text_align = 1 where id = 20 and en_name = 'DELIVERY_NAME';
update tbl_headers_center set text_align = 1 where id = 21 and en_name = 'DELIVERY_NAME_KANA';
update tbl_headers_center set text_align = 1 where id = 27 and en_name = 'CLIENT_ADDRESS1';
update tbl_headers_center set text_align = 1 where id = 28 and en_name = 'CLIENT_ADDRESS2';
update tbl_headers_center set text_align = 1 where id = 29 and en_name = 'CLIENT_NAME';
update tbl_headers_center set text_align = 1 where id = 30 and en_name = 'CLIENT_NAME_KANA';
update tbl_headers_center set text_align = 1 where id = 37 and en_name = 'COMMENT';
alter table tbl_headers_shipper drop text_align;
alter table tbl_headers_shipper add text_align integer not null default 0 COMMENT '0: center 1: left 2: right';
update tbl_headers_shipper set text_align = 1 where id = 14 and en_name = 'DELIVERY_ADDRESS1';
update tbl_headers_shipper set text_align = 1 where id = 15 and en_name = 'DELIVERY_ADDRESS2';
update tbl_headers_shipper set text_align = 1 where id = 18 and en_name = 'DELIVERY_COMPANY1';
update tbl_headers_shipper set text_align = 1 where id = 19 and en_name = 'DELIVERY_COMPANY2';
update tbl_headers_shipper set text_align = 1 where id = 20 and en_name = 'DELIVERY_NAME';
update tbl_headers_shipper set text_align = 1 where id = 21 and en_name = 'DELIVERY_NAME_KANA';
update tbl_headers_shipper set text_align = 1 where id = 27 and en_name = 'CLIENT_ADDRESS1';
update tbl_headers_shipper set text_align = 1 where id = 28 and en_name = 'CLIENT_ADDRESS2';
update tbl_headers_shipper set text_align = 1 where id = 29 and en_name = 'CLIENT_NAME';
update tbl_headers_shipper set text_align = 1 where id = 30 and en_name = 'CLIENT_NAME_KANA';
update tbl_headers_shipper set text_align = 1 where id = 37 and en_name = 'COMMENT';

-- issue-1373 alert_update_tbl_headers.sql
alter table tbl_headers_center change column property property int(1) NOT NULL DEFAULT '0' COMMENT '0:varchar, 1:halfsize number, 2:date, 3:halfsize alphabet and number';
update tbl_headers_center set property = 1 where id = 9;  -- 配達時間帯
update tbl_headers_center set property = 1 where id = 13;  -- お届け先郵便番号
update tbl_headers_center set property = 1 where id = 26;  -- ご依頼主郵便番号
update tbl_headers_center set property = 1 where id = 42;  -- 請求先顧客コード
update tbl_headers_center set property = 1 where id = 43;  -- 請求先分類コード
update tbl_headers_center set property = 3 where id = 50;  -- 配送コード
update tbl_headers_center set property = 3 where id = 51;  -- 棚番コード
alter table tbl_headers_shipper change column property property int(1) NOT NULL DEFAULT '0' COMMENT '0:varchar, 1:halfsize number, 2:date, 3:halfsize alphabet and number';
update tbl_headers_shipper set property = 1 where id = 4;  -- 送り状種類
update tbl_headers_shipper set property = 1 where id = 5;  -- クール区分
update tbl_headers_shipper set property = 1 where id = 9;  -- 配達時間帯
update tbl_headers_shipper set property = 1 where id = 13;  -- お届け先郵便番号
update tbl_headers_shipper set property = 1 where id = 26;  -- ご依頼主郵便番号
update tbl_headers_shipper set property = 1 where id = 25;  -- ご依頼主電話番号枝番
update tbl_headers_shipper set property = 1, digit_type = 1 where id = 48;  -- 配送料

-- issue-1383 alert_tbl_index_name.sql
ALTER TABLE t_shipment RENAME INDEX IDX_SHIPPER_CD TO IDX_T01_SHIPPER_CD;
ALTER TABLE t_shipment RENAME INDEX IDX_SHIPMENT_STATUS TO IDX_T01_SHIPMENT_STATUS;
ALTER TABLE t_shipment RENAME INDEX IDX_SHIPMENT_PLAN_DATE TO IDX_T01_SHIPMENT_PLAN_DATE;
ALTER TABLE t_shipment RENAME INDEX IDX_DELIVERY_PLAN_DATE TO IDX_T01_DELIVERY_PLAN_DATE;
ALTER TABLE t_shipment RENAME INDEX IDX_UPDATE_TIMESTAMP TO IDX_T01_UPDATE_TIMESTAMP;
ALTER TABLE t_shipment RENAME INDEX IDX_RE_DELIVERY_LIMIT_DATE TO IDX_T01_RE_DELIVERY_LIMIT_DATE;
ALTER TABLE t_shipment RENAME INDEX IDX_CENTER_CD TO IDX_T01_CENTER_CD;
ALTER TABLE t_shipment RENAME INDEX IDX_DELIVERY_NAME TO IDX_T01_DELIVERY_NAME;
ALTER TABLE t_shipment RENAME INDEX IDX_DELIVERY_TEL TO IDX_T01_DELIVERY_TEL;
ALTER TABLE t_shipment RENAME INDEX IDX_TERMINAL_CD TO IDX_T01_TERMINAL_CD;
ALTER TABLE t_shipment RENAME INDEX IDX_DELIVERY_TIME_FROM TO IDX_T01_DELIVERY_TIME_FROM;
ALTER TABLE t_shipment RENAME INDEX IDX_DELIVERY_TIME_TO TO IDX_T01_DELIVERY_TIME_TO;
ALTER TABLE t_shipment RENAME INDEX IDX_RE_DELIVERY_DATE TO IDX_T01_RE_DELIVERY_DATE;
ALTER TABLE t_shipment RENAME INDEX IDX_ITEM_NAME1 TO IDX_T01_ITEM_NAME1;
ALTER TABLE t_shipment RENAME INDEX IDX_ITEM_NAME2 TO IDX_T01_ITEM_NAME2;
ALTER TABLE t_shipment RENAME INDEX IDX_UPLOAD_TIMESTAMP TO IDX_T01_UPLOAD_TIMESTAMP;
ALTER TABLE t_re_delivery_request RENAME INDEX IDX_RE_DELIVERY_DATE TO IDX_T04_RE_DELIVERY_DATE;
ALTER TABLE t_re_delivery_request RENAME INDEX IDX_DELIVERY_DISTRICT_CD TO IDX_T04_DELIVERY_DISTRICT_CD;
ALTER TABLE t_re_delivery_request RENAME INDEX IDX_TERMINAL_CD TO IDX_T04_TERMINAL_CD;
ALTER TABLE t_driver_work_manage RENAME INDEX IDX_TERMINAL_CD TO IDX_T05_TERMINAL_CD;
ALTER TABLE t_driver_work_manage RENAME INDEX IDX_1 TO IDX_T05_1;
ALTER TABLE t_terminal_use_history RENAME INDEX IDX_1 TO IDX_T06_1;
ALTER TABLE t_terminal_delivery_district RENAME INDEX IDX_DELIVERY_DISTRICT_CD TO IDX_T07_DELIVERY_DISTRICT_CD;
ALTER TABLE t_terminal_delivery_district RENAME INDEX IDX_TERMINAL_USE_ID TO IDX_T07_TERMINAL_USE_ID;
ALTER TABLE m_delivery_time RENAME INDEX IDX_1 TO IDX_M03_1;
ALTER TABLE m_shipper ADD INDEX IDX_M02_1(URL_ALIAS);
